import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './User.entity';

@Entity()
export class UserLeave {
  @PrimaryGeneratedColumn()
  id: Number;

  @ApiProperty()
  @IsInt()
  @Column()
  availableLeaves: number;

  @ApiProperty()
  @IsInt()
  @Column()
  leavesTaken: number;

  @ApiProperty()
  @IsInt()
  @Column()
  totalLeaves: number;

  @ManyToOne(() => User, (user) => user.leave, { onDelete: 'CASCADE' })
  userId: User;
}
