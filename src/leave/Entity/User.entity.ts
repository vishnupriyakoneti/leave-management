import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { UserLeave } from './leave.entity';
import { UserLogin } from './login.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  leaveDate: string;

  @ApiProperty()
  @IsString()
  @Column()
  name: string;

  @ApiProperty()
  @Column()
  password: string;

  @ApiProperty()
  @IsInt()
  @Column()
  noOfDays: number;

  @OneToMany(() => UserLeave, (leave) => leave.userId, {
    cascade: true
  })
  @JoinColumn()
  leave: UserLeave[];

  @OneToOne(() => UserLogin, (reg) => reg.user, {
    cascade: true
  })
  reg: UserLogin;

  addLeave(leave: UserLeave) {
    if (this.leave == null) {
      this.leave = new Array<UserLeave>();
    }
    this.leave.push(leave);
  }
}
