import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res
} from '@nestjs/common';
import { UserLeave } from './Entity/leave.entity';
import { UserLogin } from './Entity/login.entity';
import { User } from './Entity/User.entity';
import { LeaveService } from './leave.service';
import { Response, Request } from 'express';
import { customException } from '../CustomExceptions/customExceptions';

@Controller('leave')
export class LeaveController {
  constructor(private readonly leaveService: LeaveService) {}

  @Post('/applyleave') //@ApiBody({​​type:User}​​)
  applyLeave(@Body() newdata: User) {
    return this.leaveService.applyLeave(newdata);
  }

  @Post('/login')
  async login(
    @Body() login: UserLogin,
    @Res({ passthrough: true }) response: Response
  ) {
    return this.leaveService.findOne(login, response);
  }

  @Get()
  getLeaveData(): Promise<User[]> {
    return this.leaveService.getLeaveData();
  }

  @Get('/userlogin')
  async user(@Req() request: Request) {
    return this.leaveService.findUser(request);
  }

  @Get('/:id')
  getLeavesById(@Param('id') id: number): Promise<UserLeave> {
    return this.leaveService
      .getLeavesById(id)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'Userid not found, please enter correct userid to login',
            HttpStatus.NOT_FOUND
          );
        }
      })

      .catch(() => {
        throw new customException(
          'Userid not found, please enter correct userid to login',
          HttpStatus.NOT_FOUND
        );
      });
  }

  @Put('user/:id/:date')
  update(@Param('id') id: number, @Body() date: User) {
    return this.leaveService.updateUser(id, date);
  }

  @Delete('/:id')
  delete(@Param('id') id: number) {
    return this.leaveService.deleteLeave(id);
  }

  @Post('/logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    return this.leaveService.logout(response);
  }
}

//   @Delete('user/:id/:leavedate')
//   deleteUser(@Param('id') id: number, @Param('leavedate') leavedate: string) {
//     return this.leaveService.deleteUser(id, leavedate);
//   }

//   @Delete('leave/:id/:leavedate')
//   deleteLeave(@Param('id') id: number) {
//     return this.leaveService.deleteLeave(id);
//   }
