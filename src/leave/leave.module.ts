import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserLeave } from './Entity/leave.entity';

import { User } from './Entity/User.entity';
import { LeaveController } from './leave.controller';
import { LeaveService } from './leave.service';
import { UserLogin } from './Entity/login.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, UserLeave, UserLogin]),

    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '1h' }
    })
  ],

  controllers: [LeaveController],
  providers: [LeaveService]
})
export class LeaveModule {}
