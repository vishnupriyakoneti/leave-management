import {
  BadRequestException,
  Injectable,
  Logger,
  Res,
  UnauthorizedException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository } from 'typeorm';
import { UserLeave } from './Entity/leave.entity';
import { User } from './Entity/User.entity';
import { Entity } from 'typeorm';
import { UserLogin } from './Entity/login.entity';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { Response, Request } from 'express';

@Injectable()
export class LeaveService {
  logger: Logger;
  constructor(
    @InjectRepository(UserLeave)
    private readonly leaveRepository: Repository<UserLeave>,
    @InjectRepository(User)
    private readonly dataRepository: Repository<User>,
    private jwtService: JwtService
  ) {
    this.logger = new Logger(LeaveService.name);
  }

  async getLeaveData(): Promise<User[]> {
    this.logger.log('Getting leave data');
    return await this.dataRepository.find();
  }
  public async applyLeave(newdata: User) {
    const pass = await bcrypt.hash(newdata.password, 10);
    const leavedata = new UserLeave();
    const logindata = new UserLogin();
    leavedata.totalLeaves = 15;
    leavedata.leavesTaken = newdata.noOfDays;
    leavedata.availableLeaves = leavedata.totalLeaves - leavedata.leavesTaken;

    const userdata = new User();
    userdata.name = logindata.name = newdata.name;
    userdata.leaveDate = newdata.leaveDate;
    userdata.noOfDays = newdata.noOfDays;
    userdata.password = logindata.password = pass;
    userdata.reg = logindata;
    userdata.addLeave(leavedata);
    await this.dataRepository.save(userdata);
    this.logger.log('Applying the user leave');

    return `You have successfully applied leave for ${newdata.noOfDays} days from ${newdata.leaveDate} `;
  }

  async findUser(request: Request) {
    try {
      const cookie = request.cookies['jwt'];
      const data = await this.jwtService.verifyAsync(cookie);
      if (!data) {
        throw new UnauthorizedException();
      }
      const user = await this.dataRepository.findOne({ id: data.id });
      const { password, ...result } = user;
      return result;
    } catch (e) {
      throw new UnauthorizedException();
    }
  }

  async findOne(data: UserLogin, response: Response) {
    const user = await this.dataRepository.findOne({ name: data.name });
    console.log(user);
    if (!user) {
      throw new BadRequestException('invalid credentials');
    }
    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new BadRequestException('invalid ');
    }
    const jwt = await this.jwtService.signAsync({ id: user.id });
    response.cookie('jwt', jwt, { httpOnly: true });
    return {
      message: 'success'
    };
  }

  async getLeavesById(id: number): Promise<UserLeave> {
    this.logger.log('Adding the user leaves by Id');
    return await this.leaveRepository.findOne(id);
  }

  // async  updateLeaveData( user:User, id:number) {
  //  await this.dataRepository.update(id,user);
  //    const leave= new UserLeave();
  //    leave.leavesTaken = user.noOfDays;

  //      return this.dataRepository.findOne(id);
  //    }

  //

  updateUser(id: number, data: User) {
    const user = new User();

    user.name = data.name;

    user.leaveDate = data.leaveDate;

    user.noOfDays = data.noOfDays;

    this.dataRepository.update({ id: id }, user);

    const leave = new UserLeave();

    const total = (leave.totalLeaves = 15);

    leave.leavesTaken = user.noOfDays;

    leave.availableLeaves = total - leave.leavesTaken;

    this.leaveRepository.update({ id: id }, leave);
    this.logger.log('Updating the user leave');

    return 'successfully updated';
  }

  async deleteLeave(id: number) {
    // await getConnection()

    //   .createQueryBuilder()

    //   .delete()

    //   .from(UserLeave)

    //   .where('userIdUserId= :id', { id: id })

    //   .execute();

    // await getConnection()

    //   .createQueryBuilder()

    //   .delete()

    //   .from(User)

    //   .where('userId= :id', { id: id })

    //   .execute();

    await this.dataRepository.delete(id);
    this.logger.log('Deleting the user leave by Id');

    return 'Deleted leave data succesfully';
  }

  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');
    return {
      message: 'logout success'
    };
  }

  // async deleteLeave(id: number) {
  //   await getConnection()
  //     .createQueryBuilder()
  //     .delete()
  //     .from(UserLeave)
  //     .where('userIdUserId= :id', { id: id })
  //     .execute();
  //   await getConnection()
  //     .createQueryBuilder()
  //     .delete()
  //     .from(User)
  //     .where('userId= :id', { id: id })
  //     .execute();
  //   return 'Deleted leave data succesfully';
  // }

  // async deleteUser(id:number, leavedate:string) {
  //     if(this.dataRepository.findOne(id)){
  //         await  this.dataRepository.find({leaveDate:leavedate});
  //         return this.dataRepository.delete(id);
  //     }
  //  }
  //  async deleteLeave(id:number) {

  //         return this.leaveRepository.delete(id);
  //     }
}
